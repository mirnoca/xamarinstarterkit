﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using IdentityModel.OidcClient.Browser;
using Plugin.CurrentActivity;
using Unity;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Platform = Xamarin.Essentials.Platform;

namespace XamarinStarterKit.Droid
{
    [Activity(Label = "XamarinStarterKit", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
        LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            // Platform.Init(this, savedInstanceState);
            Forms.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);

            Action<IUnityContainer> containerAction = container =>
            {
                container.RegisterType<IBrowser, ChromeTabsBrowser>();
            };

            LoadApplication(new App(containerAction));

            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                var ex = (Exception) args.ExceptionObject;
                Console.WriteLine(ex);
            };
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}