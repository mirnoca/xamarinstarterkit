﻿using System;
using Android.App;
using Android.Content;
using Android.OS;

namespace XamarinStarterKit.Droid
{
    [Activity(Label = "XamarinStarterKit")]
    [IntentFilter(new[] {Intent.ActionView},
        Categories = new[] {Intent.CategoryDefault, Intent.CategoryBrowsable},
        DataScheme = "xamarinformsclients")]
    internal class OidcAndroidCallbackActivity : Activity
    {
        public static event Action<string> Callbacks;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Callbacks?.Invoke(Intent.DataString);

            Finish();

            StartActivity(typeof(MainActivity));
        }
    }
}