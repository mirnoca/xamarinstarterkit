﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinStarterKit.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : ContentPage
    {
        public HomeView()
        {
            InitializeComponent();
        }
    }
}