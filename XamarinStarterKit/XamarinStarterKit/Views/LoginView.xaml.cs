﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinStarterKit.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : ContentPage
    {
        public LoginView()
        {
            InitializeComponent();
        }
    }
}