﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using IdentityModel.OidcClient;
using IdentityModel.OidcClient.Browser;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Unity;
using XamarinStarterKit.Provider.AppSettings;
using XamarinStarterKit.Provider.AppUser;
using XamarinStarterKit.Provider.MessageCenter;
using XamarinStarterKit.Provider.MessageCenter.Navigation;
using XamarinStarterKit.Provider.Navigation;
using XamarinStarterKit.Services.Auth;
using XamarinStarterKit.Services.Request;
using XamarinStarterKit.Services.Test;
using XamarinStarterKit.ViewModels;
using XamarinStarterKit.Views;

namespace XamarinStarterKit.Container
{
    public class AppContainer
    {
        public IUnityContainer InitContainer()
        {
            var container = new UnityContainer();

            container.RegisterTabbedView<MainView, MainViewModel>(new Dictionary<Type, Type>
            {
                {typeof(HomeView), typeof(HomeViewModel)},
                {typeof(NewsView), typeof(NewsViewModel)}
            });
            container.RegisterView<LoginView, LoginViewModel>();
            container.RegisterView<HomeView, HomeViewModel>();
            container.RegisterView<NewsView, NewsViewModel>();

            container.RegisterTypeAsSingleton<INavigationProvider, NavigationProvider>();
            container.RegisterTypeAsSingleton<ISubscriptionProvider, SubscriptionProvider>();
            container.RegisterTypeAsSingleton<INavigationSender, NavigationSender>();
            container.RegisterTypeAsSingleton<INavigationStartSender, NavigationSender>();
            container.RegisterTypeAsSingleton<IRequestService, RequestService>();
            container.RegisterTypeAsSingleton<ITestService, TestService>();

            container.RegisterFactoryAsSingleton(c =>
            {
                var appSettings = new AppSettingsProvider().Init();
                return appSettings;
            });

            container.RegisterFactoryAsSingleton(c =>
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                    NullValueHandling = NullValueHandling.Ignore
                };
                serializerSettings.Converters.Add(new StringEnumConverter());
                return serializerSettings;
            });

            container.RegisterTypeAsSingleton<HttpClientHandler>();

            container.RegisterSubscriberAsSingleton<AppStartSubscriber>();

            Auth(container);

            return container;
        }

        private void Auth(UnityContainer container)
        {
            container.RegisterTypeAsSingleton<IAppUserProvider, AppUserProvider>();
            container.RegisterTypeAsSingleton<IAuthService, AuthService>();
            container.RegisterTypeAsSingleton<ITokenService, AuthService>();
            container.RegisterFactoryAsSingleton(c =>
            {
                var appSettings = c.Resolve<AppSettings>();
                var browser = c.Resolve<IBrowser>();
                return new OidcClient(new OidcClientOptions
                {
                    Authority = appSettings.Authority,
                    ClientId = appSettings.ClientId,
                    Scope = appSettings.Scopes,
                    RedirectUri = appSettings.RedirectUri,
                    PostLogoutRedirectUri = appSettings.RedirectUri,
                    Browser = browser,
                    ResponseMode = OidcClientOptions.AuthorizeResponseMode.Redirect
                });
            });
        }
    }
}