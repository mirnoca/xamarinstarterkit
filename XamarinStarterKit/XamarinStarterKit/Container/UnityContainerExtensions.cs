﻿using System;
using System.Collections.Generic;
using Unity;
using Unity.Lifetime;
using Xamarin.Forms;
using XamarinStarterKit.Provider.MessageCenter;
using XamarinStarterKit.Provider.MessageCenter.Navigation;
using XamarinStarterKit.ViewModels.Base;

namespace XamarinStarterKit.Container
{
    public static class UnityContainerExtensions
    {
        public static void RegisterView<TView, TViewModel>(this UnityContainer container)
            where TViewModel : class, IViewModel
            where TView : ContentPage, new()
        {
            container.RegisterType<TViewModel>();
            container.RegisterFactory<TView>(c =>
            {
                var view = new TView {BindingContext = c.Resolve<TViewModel>()};
                return view;
            });
            container.RegisterSubscriberAsSingleton<NavigationSubscriber<TViewModel>>();
        }

        public static void RegisterTabbedView<TView, TViewModel>(this UnityContainer container,
            Dictionary<Type, Type> viewPerViewModel)
            where TViewModel : class, IViewModel
            where TView : TabbedPage, new()
        {
            container.RegisterType<TViewModel>();
            container.RegisterFactory<TView>(c =>
            {
                var view = new TView {BindingContext = c.Resolve<TViewModel>()};
                foreach (var viewChild in view.Children)
                {
                    var viewChildType = viewPerViewModel[viewChild.GetType()];
                    var viewModelInstance = c.Resolve(viewChildType);
                    viewChild.BindingContext = viewModelInstance;
                }

                return view;
            });
            container.RegisterSubscriberAsSingleton<NavigationSubscriber<TViewModel>>();
        }

        public static void RegisterTypeAsSingleton<TIService, TService>(this UnityContainer container)
            where TService : class, TIService
        {
            container.RegisterType<TIService, TService>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterTypeAsSingleton<TClass>(this UnityContainer container)
            where TClass : class
        {
            container.RegisterType<TClass>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterSubscriberAsSingleton<TSubscriber>(this UnityContainer container)
            where TSubscriber : ISubscriber
        {
            container.RegisterType<ISubscriber, TSubscriber>(typeof(TSubscriber).FullName,
                new ContainerControlledLifetimeManager());
        }

        public static void RegisterFactoryAsSingleton<TClass>(this UnityContainer container,
            Func<IUnityContainer, TClass> func)
            where TClass : class
        {
            container.RegisterFactory<TClass>(func, new ContainerControlledLifetimeManager());
        }
    }
}