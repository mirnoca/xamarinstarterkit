﻿using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinStarterKit.Provider.MessageCenter.Navigation;
using XamarinStarterKit.Services.Auth;
using XamarinStarterKit.ViewModels.Base;
using XamarinStarterKit.Views;

namespace XamarinStarterKit.ViewModels
{
    public class LoginViewModel : IViewModel
    {
        private readonly IAuthService _authService;
        private readonly INavigationSender _navigationSender;

        public LoginViewModel(IAuthService authService, INavigationSender navigationSender)
        {
            _authService = authService;
            _navigationSender = navigationSender;
        }

        public ICommand LoginComand => new Command(async () => await Login());

        private async Task Login()
        {
            var isAuthenticated = await _authService.Login();
            if (isAuthenticated)
            {
                _navigationSender.NavigateToView(this, nameof(MainView));
            }
        }
    }
}