﻿using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using XamarinStarterKit.Provider.AppUser;
using XamarinStarterKit.Services.Test;
using XamarinStarterKit.ViewModels.Base;

namespace XamarinStarterKit.ViewModels
{
    public class HomeViewModel : BindableObject, IViewModel
    {
        private readonly ITestService _testService;
        private readonly IAppUserProvider _userProvider;

        public HomeViewModel(ITestService testService, IAppUserProvider userProvider)
        {
            _testService = testService;
            _userProvider = userProvider;
        }

        private string _claims { get; set; }

        public ICommand CallTestApiComand => new Command(async () => await CallTestApi());

        public string UserName => _userProvider.GetClaimsPrincipalAsync().Result.Identity.Name;

        public string Claims
        {
            get => _claims;
            set
            {
                _claims = value;
                OnPropertyChanged(nameof(Claims));
            }
        }

        private async Task CallTestApi()
        {
            Claims = JArray.Parse(await _testService.CallTestApi()).ToString();
        }
    }
}