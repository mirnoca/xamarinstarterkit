﻿using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinStarterKit.Provider.MessageCenter.Navigation;
using XamarinStarterKit.Services.Auth;
using XamarinStarterKit.ViewModels.Base;
using XamarinStarterKit.Views;

namespace XamarinStarterKit.ViewModels
{
    public class MainViewModel : IViewModel
    {
        private readonly IAuthService _authService;
        private readonly INavigationSender _navigationSender;

        public MainViewModel(INavigationSender navigationSender, IAuthService authService)
        {
            _navigationSender = navigationSender;
            _authService = authService;
        }

        public ICommand LogoutCommand => new Command(async () => await Logout());

        private async Task Logout()
        {
            await _authService.Logout();
            _navigationSender.NavigateToView(this, nameof(LoginView));
        }
    }
}