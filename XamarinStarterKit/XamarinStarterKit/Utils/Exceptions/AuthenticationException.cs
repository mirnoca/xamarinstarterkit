﻿using System;
using System.Net;

namespace XamarinStarterKit.Utils.Exceptions
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(string content) : this(content, HttpStatusCode.ExpectationFailed)
        {
            Content = content;
        }

        public AuthenticationException(string content, HttpStatusCode httpCode)
        {
            Content = content;
            HttpCode = httpCode;
        }

        public string Content { get; }

        public HttpStatusCode HttpCode { get; }
    }
}