﻿using System;
using System.Net;
using System.Net.Http;

namespace XamarinStarterKit.Utils.Exceptions
{
    internal class HttpCodeRequestException : HttpRequestException
    {
        public HttpCodeRequestException(HttpStatusCode code, string message, Exception inner) : base(message,
            inner)
        {
            HttpCode = code;
        }

        public HttpStatusCode HttpCode { get; }
    }
}