﻿namespace XamarinStarterKit.Provider.MessageCenter
{
    public interface ISubscriptionProvider
    {
        void InitSubscribers();
    }
}