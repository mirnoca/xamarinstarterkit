﻿using Xamarin.Forms;
using XamarinStarterKit.Provider.Navigation;

namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public class AppStartSubscriber : ISubscriber
    {
        private readonly INavigationProvider _navigationProvider;

        public AppStartSubscriber(INavigationProvider navigationProvider)
        {
            _navigationProvider = navigationProvider;
        }

        public void Init()
        {
            MessagingCenter.Subscribe<App, NavigationMessage>(this, MessageKeys.NavigateToView,
                async (sender, message) => { await _navigationProvider.NavigateToAsync(message.View); });
        }
    }
}