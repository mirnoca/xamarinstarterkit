﻿using Xamarin.Forms;
using XamarinStarterKit.ViewModels.Base;

namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public class NavigationSender : INavigationSender, INavigationStartSender
    {
        public void NavigateToView<TViewModel>(TViewModel viewModel, string view)
            where TViewModel : class, IViewModel
        {
            MessagingCenter.Send(viewModel, MessageKeys.NavigateToView,
                new NavigationMessage {View = view});
        }

        public void NavigateToView(App app, string view)
        {
            MessagingCenter.Send(app, MessageKeys.NavigateToView, new NavigationMessage {View = view});
        }
    }
}