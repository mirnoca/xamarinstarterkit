﻿namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public interface INavigationStartSender
    {
        void NavigateToView(App app, string view);
    }
}