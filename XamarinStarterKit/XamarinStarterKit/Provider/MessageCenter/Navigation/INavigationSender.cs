﻿using XamarinStarterKit.ViewModels.Base;

namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public interface INavigationSender
    {
        void NavigateToView<TViewModel>(TViewModel viewModel, string view) where TViewModel : class, IViewModel;
    }
}