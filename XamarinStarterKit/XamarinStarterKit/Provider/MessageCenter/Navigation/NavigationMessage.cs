﻿namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public class NavigationMessage
    {
        public string View { get; set; }
    }
}