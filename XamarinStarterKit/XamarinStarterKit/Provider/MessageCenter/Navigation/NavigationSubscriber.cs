﻿using Xamarin.Forms;
using XamarinStarterKit.Provider.Navigation;
using XamarinStarterKit.ViewModels.Base;

namespace XamarinStarterKit.Provider.MessageCenter.Navigation
{
    public class NavigationSubscriber<TViewModel> : ISubscriber where TViewModel : class, IViewModel
    {
        private readonly INavigationProvider _navigationProvider;

        public NavigationSubscriber(INavigationProvider navigationProvider)
        {
            _navigationProvider = navigationProvider;
        }

        public void Init()
        {
            MessagingCenter.Subscribe<TViewModel, NavigationMessage>(this, MessageKeys.NavigateToView,
                async (sender, message) =>
                {
                    await _navigationProvider.NavigateToAsync(message.View);
                    await _navigationProvider.RemoveLastFromBackStackAsync();
                });
        }
    }
}