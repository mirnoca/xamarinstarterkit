﻿namespace XamarinStarterKit.Provider.MessageCenter
{
    public interface ISubscriber
    {
        void Init();
    }
}