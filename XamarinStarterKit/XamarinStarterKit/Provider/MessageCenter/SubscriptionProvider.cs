﻿using System.Collections.Generic;

namespace XamarinStarterKit.Provider.MessageCenter
{
    public class SubscriptionProvider : ISubscriptionProvider
    {
        private readonly IEnumerable<ISubscriber> _subscribers;

        public SubscriptionProvider(IEnumerable<ISubscriber> subscribers)
        {
            _subscribers = subscribers;
        }

        public void InitSubscribers()
        {
            foreach (var subscriber in _subscribers)
            {
                subscriber.Init();
            }
        }
    }
}