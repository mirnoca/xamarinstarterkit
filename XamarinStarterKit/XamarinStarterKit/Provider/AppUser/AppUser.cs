﻿using System.Security.Claims;

namespace XamarinStarterKit.Provider.AppUser
{
    public class AppUser
    {
        public AppUser(ClaimsPrincipal user, Tokens tokens)
        {
            User = user;
            Tokens = tokens;
        }

        public ClaimsPrincipal User { get; }
        public Tokens Tokens { get; }
    }
}