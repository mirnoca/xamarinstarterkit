﻿using System;

namespace XamarinStarterKit.Provider.AppUser
{
    public class Tokens
    {
        public Tokens(string accessToken, string refreshToken, string identityToken, DateTime? accessTokenExpiration)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            IdentityToken = identityToken;
            AccessTokenExpiration = accessTokenExpiration;
        }

        public string AccessToken { get; }
        public string RefreshToken { get; }
        public string IdentityToken { get; }
        public DateTime? AccessTokenExpiration { get; }
    }
}