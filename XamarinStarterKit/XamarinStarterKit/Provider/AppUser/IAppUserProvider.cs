﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.OidcClient;

namespace XamarinStarterKit.Provider.AppUser
{
    public interface IAppUserProvider
    {
        Task<bool> IsAuthenticatedAsync();
        Task<Tokens> GetTokensAsync();
        Task<ClaimsPrincipal> GetClaimsPrincipalAsync();
        Task UpdateAppUserAsync(LoginResult result);

        Task UpdateTokens(string accessToken, string refreshToken, string identityToken,
            DateTime? accessTokenExpiration);
    }
}