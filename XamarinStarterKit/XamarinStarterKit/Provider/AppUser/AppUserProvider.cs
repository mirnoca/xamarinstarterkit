﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.OidcClient;
using Xamarin.Forms;

namespace XamarinStarterKit.Provider.AppUser
{
    public class AppUserProvider : IAppUserProvider
    {
        public async Task<bool> IsAuthenticatedAsync()
        {
            var tokens = await GetValueOrDefault<Tokens>(nameof(AppUser.Tokens));
            return tokens != null && tokens.AccessTokenExpiration > DateTime.Now;
        }

        public async Task<Tokens> GetTokensAsync()
        {
            return await GetValueOrDefault<Tokens>(nameof(AppUser.Tokens));
        }

        public async Task<ClaimsPrincipal> GetClaimsPrincipalAsync()
        {
            return await GetValueOrDefault<ClaimsPrincipal>(nameof(AppUser.User));
        }

        public async Task UpdateAppUserAsync(LoginResult result)
        {
            await UpdateDictionary(nameof(AppUser.User), result?.User);
            await UpdateTokens(result?.AccessToken, result?.RefreshToken, result?.IdentityToken,
                result?.AccessTokenExpiration);
        }

        public async Task UpdateTokens(string accessToken, string refreshToken, string identityToken,
            DateTime? accessTokenExpiration)
        {
            await UpdateDictionary(nameof(AppUser.Tokens),
                new Tokens(accessToken, refreshToken, identityToken, accessTokenExpiration)
            );
        }

        private async Task UpdateDictionary<T>(string key, T value)
        {
            try
            {
                Application.Current.Properties[key] = value;
                await Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to update: {key} Message: {ex.Message}");
            }
        }

        private async Task<T> GetValueOrDefault<T>(string key, T defaultValue = default)
        {
            object value = null;
            if (Application.Current.Properties.ContainsKey(key))
            {
                value = Application.Current.Properties[key];
            }

            return await Task.FromResult(null != value ? (T) value : defaultValue);
        }
    }
}