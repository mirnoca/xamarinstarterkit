﻿using System.Threading.Tasks;

namespace XamarinStarterKit.Provider.Navigation
{
    public interface INavigationProvider
    {
        Task NavigateToAsync(string view);

        Task RemoveLastFromBackStackAsync();
    }
}