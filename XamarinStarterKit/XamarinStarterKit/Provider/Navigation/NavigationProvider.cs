﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Unity;
using Xamarin.Forms;
using XamarinStarterKit.Views;

namespace XamarinStarterKit.Provider.Navigation
{
    public class NavigationProvider : INavigationProvider
    {
        private readonly IUnityContainer _container;

        public NavigationProvider(IUnityContainer container)
        {
            _container = container;
        }

        public async Task NavigateToAsync(string view)
        {
            Page CreateViewAsync()
            {
                var viewType = typeof(MainView).GetTypeInfo().Assembly.GetTypes().SingleOrDefault(t => t.Name == view);
                if (viewType == null)
                {
                    throw new Exception($"Cannot locate view type for {view}");
                }

                return _container.Resolve(viewType) as Page;
            }

            var page = CreateViewAsync();

            if (Application.Current.MainPage is CustomNavigationView navigationPage)
            {
                await navigationPage.PushAsync(page);
                NavigationPage.SetHasBackButton(page, false);
            }
            else
            {
                Application.Current.MainPage = new CustomNavigationView(page);
            }
        }

        public async Task RemoveLastFromBackStackAsync()
        {
            if (!(Application.Current.MainPage is CustomNavigationView mainPage))
            {
                return;
            }

            var count = mainPage.Navigation.NavigationStack.Count;
            if (count >= 2)
            {
                mainPage.Navigation.RemovePage(mainPage.Navigation.NavigationStack[count - 2]);
            }

            await Task.FromResult(true);
        }
    }
}