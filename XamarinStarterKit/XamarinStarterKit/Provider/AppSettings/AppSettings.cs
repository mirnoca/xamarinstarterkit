﻿namespace XamarinStarterKit.Provider.AppSettings
{
    public class AppSettings
    {
        public bool AuthenticationActivated { get; set; }
        public string Authority { get; set; }
        public string ClientId { get; set; }
        public string Scopes { get; set; }
        public string RedirectUri { get; set; }
        public string TestApi { get; set; }
    }
}