﻿using System;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace XamarinStarterKit.Provider.AppSettings
{
    public class AppSettingsProvider
    {
        public AppSettings Init()
        {
            try
            {
                var appTypeInfo = typeof(App).GetTypeInfo();
                var filename = "appsettings.json";
                var stream = appTypeInfo.Assembly.GetManifestResourceStream($"{appTypeInfo.Namespace}.{filename}")
                             ?? throw new Exception("appsettings not found");

                using (var reader = new StreamReader(stream))
                {
                    var json = reader.ReadToEnd();
                    var result = JsonConvert.DeserializeObject<AppSettings>(json);

                    return result;
                }
            }
            catch (Exception e)
            {
                throw new Exception("Something went wrong read appsettings!", e);
            }
        }
    }
}