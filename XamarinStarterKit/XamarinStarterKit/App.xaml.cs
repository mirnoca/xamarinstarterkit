﻿using System;
using Unity;
using Xamarin.Forms;
using XamarinStarterKit.Container;
using XamarinStarterKit.Provider.AppSettings;
using XamarinStarterKit.Provider.AppUser;
using XamarinStarterKit.Provider.MessageCenter;
using XamarinStarterKit.Provider.MessageCenter.Navigation;
using XamarinStarterKit.Views;

namespace XamarinStarterKit
{
    public partial class App : Application
    {
        public App(Action<IUnityContainer> androidContainer)
        {
            InitializeComponent();

            var container = new AppContainer().InitContainer();
            androidContainer(container);

            InitSubscribers(container);
            AppStart(container);
        }

        protected override void OnStart() { }

        protected override void OnSleep() { }

        protected override void OnResume() { }

        private void InitSubscribers(IUnityContainer container)
        {
            var provider = container.Resolve<ISubscriptionProvider>();
            provider.InitSubscribers();
        }

        private void AppStart(IUnityContainer container)
        {
            var authenticationActivated = container.Resolve<AppSettings>().AuthenticationActivated;
            var isAuthenticated = container.Resolve<IAppUserProvider>().IsAuthenticatedAsync().Result;
            var view = !authenticationActivated || isAuthenticated
                ? nameof(MainView)
                : nameof(LoginView);
            container.Resolve<INavigationStartSender>().NavigateToView(this, view);
        }
    }
}