﻿using System.Threading.Tasks;

namespace XamarinStarterKit.Services.Request
{
    public interface IRequestService
    {
        Task<TResult> GetAsync<TResult>(string address);

        Task<string> GetAsync(string address);

        Task<TResult> PostAsync<TResult, TModel>(string address, TModel model);
    }
}