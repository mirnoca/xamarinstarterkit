﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using XamarinStarterKit.Services.Auth;
using XamarinStarterKit.Utils.Exceptions;

namespace XamarinStarterKit.Services.Request
{
    public class RequestService : IRequestService
    {
        private readonly HttpClientHandler _httpClientHandler;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly ITokenService _tokenService;

        public RequestService(JsonSerializerSettings serializerSettings, HttpClientHandler httpClientHandler,
            ITokenService tokenService)
        {
            _serializerSettings = serializerSettings;
            _httpClientHandler = httpClientHandler;
            _tokenService = tokenService;
        }

        public async Task<string> GetAsync(string address)
        {
            var content = await CallAsync(client => client.GetAsync(address));
            return await content.ReadAsStringAsync();
        }

        public async Task<TResult> GetAsync<TResult>(string address)
        {
            var content = await CallAsync(client => client.GetAsync(address));
            var serialized = await content.ReadAsStringAsync();
            return await TryParseJson<TResult>(serialized);
        }

        public async Task<TResult> PostAsync<TResult, TModel>(string address, TModel model)
        {
            var postContent =
                new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

            var content = await CallAsync(client => client.PostAsync(address, postContent));
            var serialized = await content.ReadAsStringAsync();
            return await TryParseJson<TResult>(serialized);
        }

        private async Task<HttpContent> CallAsync(Func<HttpClient, Task<HttpResponseMessage>> requestMethod)
        {
            using (var client = new HttpClient(_httpClientHandler, false))
            {
                var accessToken = await _tokenService.GetAccessToken();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                client.Timeout = TimeSpan.FromMilliseconds(3000);

                var response = await requestMethod(client);
                if (!response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    if (response.StatusCode == HttpStatusCode.Forbidden
                        || response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new AuthenticationException(content, response.StatusCode);
                    }

                    throw new HttpCodeRequestException(response.StatusCode, content, null);
                }

                return response.Content;
            }
        }

        private async Task<TResult> TryParseJson<TResult>(string serialized)
        {
            try
            {
                return await Task.Run(() =>
                    JsonConvert.DeserializeObject<TResult>(serialized, _serializerSettings));
            }
            catch (JsonReaderException e)
            {
                return default;
            }
        }
    }
}