﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using IdentityModel.OidcClient;
using XamarinStarterKit.Provider.AppUser;

namespace XamarinStarterKit.Services.Auth
{
    public class AuthService : IAuthService, ITokenService
    {
        private readonly IAppUserProvider _appUserProvider;
        private readonly OidcClient _client;

        public AuthService(OidcClient client, IAppUserProvider appUserProvider)
        {
            _client = client;
            _appUserProvider = appUserProvider;
        }

        public async Task<bool> Login()
        {
            if (await _appUserProvider.IsAuthenticatedAsync())
            {
                return await Task.FromResult(true);
            }

            var result = await _client.LoginAsync(new LoginRequest());
            if (result.IsError)
            {
                throw new Exception(result.Error);
            }

            await _appUserProvider.UpdateAppUserAsync(result);

            return await Task.FromResult(true);
        }

        public async Task Logout()
        {
            var result = await _client.LogoutAsync(new LogoutRequest
            {
                IdTokenHint = (await _appUserProvider.GetTokensAsync()).IdentityToken
            });
            if (result.IsError)
            {
                throw new Exception(result.Error);
            }

            await _appUserProvider.UpdateAppUserAsync(null);
        }

        public async Task<string> GetAccessToken()
        {
            if (await _appUserProvider.IsAuthenticatedAsync())
            {
                return (await _appUserProvider.GetTokensAsync()).AccessToken;
            }

            var tokens = await _appUserProvider.GetTokensAsync();
            if (string.IsNullOrEmpty(tokens.RefreshToken))
            {
                throw new AuthenticationException("Please Login!");
            }

            var result = await _client.RefreshTokenAsync(tokens.RefreshToken);
            await _appUserProvider.UpdateTokens(result.AccessToken, result.RefreshToken, result.IdentityToken,
                result.AccessTokenExpiration);
            return result.AccessToken;
        }
    }
}