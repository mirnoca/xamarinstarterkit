﻿using System.Threading.Tasks;

namespace XamarinStarterKit.Services.Auth
{
    public interface IAuthService
    {
        Task<bool> Login();

        Task Logout();
    }
}