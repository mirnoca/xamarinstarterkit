﻿using System.Threading.Tasks;

namespace XamarinStarterKit.Services.Auth
{
    public interface ITokenService
    {
        Task<string> GetAccessToken();
    }
}