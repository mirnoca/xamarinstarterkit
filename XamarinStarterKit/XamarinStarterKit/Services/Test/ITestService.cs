﻿using System.Threading.Tasks;

namespace XamarinStarterKit.Services.Test
{
    public interface ITestService
    {
        Task<string> CallTestApi();
    }
}