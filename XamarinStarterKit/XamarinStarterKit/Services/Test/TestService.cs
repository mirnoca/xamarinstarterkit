﻿using System.Threading.Tasks;
using XamarinStarterKit.Provider.AppSettings;
using XamarinStarterKit.Services.Request;

namespace XamarinStarterKit.Services.Test
{
    public class TestService : ITestService
    {
        private readonly AppSettings _appSettings;
        private readonly IRequestService _requestService;

        public TestService(IRequestService requestService, AppSettings appSettings)
        {
            _requestService = requestService;
            _appSettings = appSettings;
        }

        public async Task<string> CallTestApi()
        {
            return await _requestService.GetAsync(_appSettings.TestApi);
        }
    }
}