# XamarinStarterKit

This is a basic Xamarin Forms application. 
This example includes 
- authentication with OpenID Connect Client for Identity Server 4
- dependency injection with Unity
- usage of icons with Font Awesome